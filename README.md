![icon](icon.png)
# flatline
a simple prompt for zsh.

## showcase
![flatline showcase](showcase.png)

* Terminal: [iTerm 2](https://iterm2.com)
* Color scheme: [Catppuccin Mocha](https://github.com/catppuccin/catppuccin)
* Font: [Monocraft](https://github.com/IdreesInc/Monocraft)

## modules
flatline shows different modules on your prompt

 * virtualenv *(magenta)*
 * username *(cyan)*
 * directory *(blue)*
 * git branch and changes *(green/yellow)*
 * command failure *(red)*
 * root status *($ or #)*

## installing
to install flatline, you can either add the `prompt.sh` snippet to your .zshrc or use the install script:

```sh
source <(curl -Ls https://gitlab.com/jancraft888/flatline/-/raw/main/install.sh)
```
