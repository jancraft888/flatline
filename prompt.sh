# flatline prompt
autoload -U colors && colors
autoload -Uz vcs_info
precmd() {
        success=$?
        vcs_info
        if [[ -n "$VIRTUAL_ENV" ]]; then
          prompt="%{$fg[magenta]%}<$(basename $VIRTUAL_ENV)> "
        else
          prompt=""
        fi

        if [[ -n ${vcs_info_msg_0_} && `git status --porcelain` ]]; then
          prompt+="%{$fg[cyan]%}<$(whoami)> %{$fg[blue]%}<%~>%{$fg[yellow]%}${vcs_info_msg_0_} "
        else
          prompt+="%{$fg[cyan]%}<$(whoami)> %{$fg[blue]%}<%~>%{$fg[green]%}${vcs_info_msg_0_} "
        fi
        if [ $success -ne 0 ]; then
                prompt+="%{$fg[red]%}<!> "
        fi
        if [ $EUID -ne 0 ]; then
                prompt+="%{$reset_color%}$ "
        else
                prompt+="%{$reset_color%}# "
        fi
        true
}

zstyle ':vcs_info:*' enable git svn
zstyle ':vcs_info:git*' formats ' <%b>'

setopt prompt_subst
precmd
