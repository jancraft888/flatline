# flatline install script
if [ -d "$HOME/flatline" ]; then
  curl -Ls https://gitlab.com/jancraft888/flatline/-/raw/main/prompt.sh -o ~/flatline/prompt.sh
  echo "flatline has been updated!"
  echo "have fun!"
  source ~/flatline/prompt.sh
else
  mkdir -p ~/flatline/
  curl -Ls https://gitlab.com/jancraft888/flatline/-/raw/main/prompt.sh -o ~/flatline/prompt.sh
  echo "" >> ~/.zshrc
  echo "# flatline prompt <3" >> ~/.zshrc
  echo "source ~/flatline/prompt.sh" >> ~/.zshrc
  echo "flatline has been installed!"
  echo "have fun!"
  source ~/flatline/prompt.sh
fi
